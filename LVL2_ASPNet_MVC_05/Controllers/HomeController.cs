﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_05.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            try
            {
                int a, b, c;
                a = 10;
                b = 0;
                c = a / b;
                ViewBag.Result = c;
            }
            catch
            {
                ViewBag.Result = "Error karena pembagian 0";
            }
            
            return View();
        }
    }
}